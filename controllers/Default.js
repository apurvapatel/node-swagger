'use strict';

var utils = require('../utils/writer.js');
var Default = require('../service/DefaultService');

module.exports.helloworldGET = function helloworldGET (req, res, next) {
  Default.helloworldGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
