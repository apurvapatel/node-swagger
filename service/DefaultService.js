'use strict';


/**
 * A simple hellow world service
 * Hello
 *
 * returns String
 **/
exports.helloworldGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = "Hello world!";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

